map = dict()
map['s'] = {'a', 'c'}
map['a'] = {'b', 'e'}
map['b'] = {'c', 'd', 'g1'}
map['c'] = {'d', 'g2'}
map['d'] = {'g1', 'g2'}
map['e'] = {'g1', 'g2'}
map['g1'] = {}
map['g2'] = {}



class Stack:
    def __init__(self):
        self.stack = list()
    def add(self, x):
        self.stack.append(x)
    def pop(self):
        return self.stack.pop(-1)
    def get_len(self):
        return len(self.stack)

class MapNode:
    def __init__(self, parent, name):
        self.parent = parent
        self.name = name

def DFS(Map):
    stack = Stack()
    closed = set()
    root = MapNode(None, "s")
    stack.add(root)
    closed.add(root.name)
    while stack.get_len() != 0:
        current_Node = stack.pop()
        if current_Node.name == "g1" or current_Node.name == "g2":
            return find_path(current_Node)
        successors = Map[current_Node.name]
        for successor in successors:
            successor_Node = MapNode(current_Node.name, successor)
            if successor not in closed:
                stack.add(successor_Node)
                closed.add(successor)


def find_path(goal_node):
    result = list()
    while goal_node is not None:
        print(1)
        print(goal_node.parent)
        result.append(goal_node.name)

        goal_node = goal_node.parent
    result.reverse()
    return result


print(DFS(map))
