map = dict()
map['s'] = {'a': 2, 'c': 2}
map['a'] = {'b': 1, 'e': 8}
map['b'] = {'c': 1, 'd': 1, 'g1': 4}
map['c'] = {'d': 1, 'g2': 5}
map['d'] = {'g1': 5, 'g2': 3}
map['e'] = {'g1': 4, 'g2': 7}
map['g1'] = {}
map['g2'] = {}


class Queue:
    def __init__(self):
        self.queue = list()

    def enqueue(self, x):
        self.queue.append(x)

    def dequeue(self):
        min_ind = 0
        for i in range(len(self.queue)):
            if self.queue[i].parent is not None:
                if self.queue[i].distance < self.queue[min_ind].distance:
                    min_ind = i
        node = self.queue[min_ind]
        del self.queue[min_ind]
        return node

    def get_length(self):
        return len(self.queue)

    def get_existing_node(self, node_name):
        for i in self.queue:
            if i.name == node_name:
                return i

    def delete_existing(self, node_name):
        for node in self.queue:
            if node.name == node_name:
                del node


class MapNode:
    def __init__(self, parent, name, distance=0):
        self.parent = parent
        self.name = name
        self.distance = distance


def UCS(Map):
    queue = Queue()
    closed = list()
    root_node = MapNode(None, 's')
    queue.enqueue(root_node)
    closed.append(root_node.name)
    while queue.get_length() != 0:
        current_node = queue.dequeue()
        closed.remove(current_node.name)
        if current_node.name == 'g1' or current_node.name == 'g2':
            return find_path(current_node)
        successors = map[current_node.name]
        for successor in successors:
            distance = current_node.distance + map[current_node.name][successor]
            print(distance)
            successor_node = MapNode(current_node, successor, distance)
            print("closed", closed)
            if successor in closed:
                existing_node = queue.get_existing_node(successor)
                if successor_node.distance < existing_node.distance:
                    queue.delete_existing(successor)
            queue.enqueue(successor_node)
            closed.append(successor)


def find_path(goal_node):
    result = []
    while goal_node != None:
        result.append(goal_node.name)
        goal_node = goal_node.parent
    result.reverse()
    return result


print(UCS(map))
