
map = dict()
map['s'] = {'a': 2, 'c': 2}
map['a'] = {'b': 1, 'e': 8}
map['b'] = {'c': 1, 'd':1, 'g1':4}
map['c'] = {'d': 1, 'g2': 5}
map['d'] = {'g1': 5, 'g2': 3}
map['e'] = {'g1': 4, 'g2': 7}
map['g1'] = {}
map['g2'] = {}

distance = dict()
map['sa'] = {2}

# import queue as Q
# queue = Q.PriorityQueue()

class priorityQueue:
    def __init__(self):
        self.queue = list()
    def get_length(self):
        return len(self.queue)
    def enqueue(self, x):
        self.queue.append(x)
    # for popping an element based on Priority
    def dequeue(self):
        max_ind = 0
        for i in range(len(self.queue)):
            print("i", i)
            if self.queue[i].parent is not None:
                print(map[self.queue[i].parent.name][self.queue[i].name])
                print(map[self.queue[max_ind].parent.name][self.queue[max_ind].name])
                if map[self.queue[i].parent.name][self.queue[i].name] < map[self.queue[max_ind].parent.name][self.queue[max_ind].name]:
                    max_ind = i

        item = self.queue[max_ind]
        del self.queue[max_ind]
        return item



class MapNode:
    def __init__(self, parent, name):
        self.name = name
        self.parent = parent

def BFS(Map):
    queue = priorityQueue()
    closed = set()
    root_node = MapNode(None, 's')
    queue.enqueue(root_node)
    closed.add(root_node.name)
    while queue.get_length() != 0:
        current_node = queue.dequeue()
        if current_node.name == 'g1' or current_node.name == 'g2':
            return find_path(current_node)
        successors = Map[current_node.name]
        for successor in successors:
            successor_node = MapNode(current_node, successor)
            queue.enqueue(successor_node)

def find_path(goal_node):
    result = []
    while goal_node != None:
        result.append(goal_node.name)
        goal_node = goal_node.parent
    result.reverse()
    return result

print(BFS(map))