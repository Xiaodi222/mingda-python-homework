
class Node:
    left = None
    right = None
    parent = None
    value = None

    def __init__(self, left, right, parent, value):
        self.left = left
        self.right = right
        self.parent = parent
        self.value = value

    def add(self, x):
        if self.value is None:
            self.value = x
        else:
            if x < self.value:
                if self.left is None: # 如果当前的左节点是None
                    self.left = Node(None, None, self, x)
                else: # 如果当前的左节点已经被定义（已经有一个数字在左边了）， 递归的往左边添加数字
                    self.left.add(x)
            elif x > self.value:
                if self.right is None:
                    self.right = Node(None, None, self, x)
                else:
                    self.right.add(x)
            else:
                return

    def beautiful_print(self):
        left_string = 'None'
        right_string = 'None'
        self_string = 'None'

        if self.left is not None:
            left_string = self.left.beautiful_print()
        if self.right is not None:
            right_string = self.right.beautiful_print()
        if self.value is not None:
            self_string = str(self.value)
        return left_string + ', ' + self_string + ', ' + right_string

    def remove(self, x):
        # 第一步：找到需要remove的点
        parent_node = None
        current_node = self # 当前节点
        LorR = None
        while current_node.value != x:
            if x < current_node.value:
                parent_node = current_node
                current_node = current_node.left
                LorR = 'left'
            elif x > current_node.value:
                parent_node = current_node
                current_node = current_node.right
                LorR = 'right'
            if current_node is None:
                # x不在树中
                return

        # 出了while循环的时候，current_node就是我们需要删除的node
        # 第二步，删除当前节点（current_node)
        # Case1: 叶节点，找到父节点，让其左/右子节点直接变为None
        if current_node.left is None and current_node.right is None:
            if LorR == 'left':
                parent_node.left = None
            elif LorR == 'right':
                parent_node.right = None
        # Case2: 中间节点，但是只有一个子节点
        elif current_node.left is None and current_node.right is not None:
            # 只有右子节点
            parent_node.right = current_node.right
        elif current_node.right is None and current_node.left is not None:
            # 只有左子节点
            parent_node.left = current_node.left
        # Case3: 中间节点，但是左右子节点都存在，删除left most
        else:
            parent2 = current_node
            left_most = current_node.left
            while left_most.right != None:
                parent2 = left_most
                left_most = left_most.right
            current_node.value = left_most.value
            if current_node.left.right is None:
                current_node.left = current_node.left.left
            else:
                parent2.right = None



    def height(self):
        """
        求出树高
        :return:
        """
        left_height = 0
        right_height = 0
        if self.left is not None:
            print("l",self.left.value,left_height)
            left_height = self.left.height()+1

        if self.right is not None:
            print("r",self.right.value,right_height)
            right_height = self.right.height()+1

        return max(left_height, right_height)


    def get_max(self):
        #树中最大的数字
        if self.right is None:
            print(self.value)
            #print(parent.value)
            return self.value
        elif self.right is not None:
            print(self.value)
            #parent = self
            return self.right.get_max()



    def get_min(self):
        if self.left is None:
            print(self.value)
            return self.value
        elif self.left is not None:
            return self.left.get_min()

    def get_num(self):
        #树中节点个数
        count = 1
        left_count = 0
        right_count = 0
        if self.left is not None:
            left_count += self.left.get_num()+1
        if self.right is not None:
            right_count += self.right.get_num()+1
        # if self.value is not None:
        #     print(3,self.value)
        #     count += 1
        return left_count + right_count + count

    def inside(self, num):
        if self.left is None and self.right is None and self.value != num:
            return False
        if num < self.value:
            if self.left is None:
                return False
            else:
                return self.left.inside(num)
        elif num > self.value:
            if self.right is None:
                return False
            else:
                return self.right.inside(num)
        else:
            return True

node = Node(None, None, None, None)
node.add(6)
node.add(3)
node.add(1)
node.add(4)
node.add(2)
node.add(7)
node.add(8)



print(node.beautiful_print())
node.remove(3)
print(node.beautiful_print())

# print(node.height())
print(node.get_num())
print(node.inside(3))







