

class LinkedList():
    value = None # 存的数字
    next = None # 下一个对象

    def __init__(self):
        pass

    # 3,5,1,2,4,7,9
    # add(10,4)
    # 3,5,1,2,10,4,7,9

    def inside(self, num):
        current_object = self
        while current_object.value != num:
            current_object = current_object.next
            if current_object is None:
                return False
        return True

    def add(self, num, index=None):
        if self.value is None:
            self.value = num
        else:
            new_LL = LinkedList() # 新的对象 10
            new_LL.value = num
            current_LL = self
            if index is None:
                while current_LL.next is not None:
                    current_LL = current_LL.next
                current_LL.next = new_LL # 新对象连接上老对象的最后一个
            else:
                temp = 1
                while temp != index:
                    current_LL = current_LL.next # 2
                    temp += 1
                new_next = current_LL.next # 4
                current_LL.next = new_LL
                new_LL.next = new_next


    def remove(self, num):
        # 找到数字的对象在哪
        current_LL = self
        pre_LL = None
        while current_LL.value != num:
            pre_LL = current_LL
            current_LL = current_LL.next
        # 删除对象B(current_LL)，并且把前一个A(pre_LL)和后一个C(next_LL)重新连上
        next_LL = current_LL.next
        pre_LL.next = next_LL

    def to_string(self):
        res = ''
        current_LL = self
        while current_LL is not None:
            res += str(current_LL.value) + ' || '
            current_LL = current_LL.next
        return res

    def count(self):
        current_LL = self
        count_LL = 0
        while count_LL is not None:
            count_LL += 1
            current_LL = current_LL.next
        return count_LL
#
# LL = LinkedList()
# LL.add(5)
# LL.add(1)
# LL.add(2)
# LL.add(4)
# LL.add(7)
# LL.add(9)
# print(LL.to_string())
# LL.add(10,4)
# print(LL.to_string())
# LL.remove(10)
# print(LL.to_string())