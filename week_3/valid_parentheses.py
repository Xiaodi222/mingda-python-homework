

def valid_parentheses(input):
    """
    输入是一串各种括号的字符串，包括小括号，中括号，大括号。括号使用规则与数学括号相同，即必须在正确的位置成对出现
    比如()就是正确格式，[()]也是正确格式
    但是[(])就不属于正确格式
    该函数要求判断输入的符号是否是正确格式的，正确返回True，否则返回False。
    :param input:
    :return:
    """
    list_parentheses = []
    dict_parentheses = {")":"(","]":"[","}":"{"}
    for parentheses in input:
        if parentheses in dict_parentheses.values():
            list_parentheses.append(parentheses)
        elif parentheses in dict_parentheses.keys():
            if list_parentheses == []:
                return False
            if list_parentheses.pop(-1) != dict_parentheses[parentheses]:
                return False
    return True

print(valid_parentheses("[()]"))