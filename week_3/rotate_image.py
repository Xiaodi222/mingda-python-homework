

def rotate_image(input):
    """
    题目在https://leetcode.com/problems/rotate-image/
    可以直接在网站上写代码测试代码正确性
    :param input:
    :return:
    """
    new_matrix = []
    for column in range(len(input)):
        new_matrix.append([])
        for row in range(len(input)-1,-1,-1):
            print(row,column)
            new_matrix[column].append(input[row][column])
    return new_matrix


matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print(rotate_image(matrix))