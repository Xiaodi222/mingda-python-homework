

def count_characters2(input):
    """
    输入一个字符串，统计总共含有多少种不同类型的符号，并返回类型个数
    例如，数字字符串"abcc"，则返回3
    :param input:
    :return:
    """
    new_set = set()
    for character in input:
        new_set.add(character)
    return len(new_set)

print(count_characters2("mabccd"))
