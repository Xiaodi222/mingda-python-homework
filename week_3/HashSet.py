from LinkedList import LinkedList


class HashSet:
    # 规定这个set只能储存英文字符串
    max_length = 8 # 坑的个数

    def __init__(self):
        self.table = list() # 初始化坑
        self.initialize()

    def initialize(self):
        for i in range(self.max_length):
            self.table.append(LinkedList())

    def get_hash_value(self, input):
        # 哈希函数
        return input ** 5 % 8
        # value = 0
        # for character in input:
        #     value += ord(character)
        # value = value % 8
        # return value

    def add(self, input):
        hash_value = self.get_hash_value(input)
        if not self.inside(input):
            self.table[hash_value].add(input)

    def remove(self, input):
        hash_value = self.get_hash_value(input)
        if self.inside(input):
            self.table[hash_value].remove(input)

    def inside(self, input):
        hash_value = self.get_hash_value(input)
        LL = self.table[hash_value]
        return LL.inside(input)

    def count(self):
        count_LL = 0
        for LL in self.table:
            count_LL += LL.count()
        return count_LL

    def to_string(self):
        res = '{'
        for LL in self.table:
            current_LL = LL
            if current_LL.value is None:
                continue
            while current_LL is not None:
                res += str(current_LL.value)
                res += ', '
                current_LL = current_LL.next
        res = res[:-2]
        res += '}'
        return res

num_set = HashSet()
num_set.add(2)
num_set.add(10)
num_set.add(1)
num_set.add(5)
num_set.add(8)
print(num_set.to_string())