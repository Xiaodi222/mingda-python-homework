

def generateMatrix(num):
	"""
	题目在https://leetcode.com/problems/spiral-matrix-ii/
	Given a positive integer n, generate an n x n matrix filled with elements from 1 to n**2 in spiral order.
	Input: n = 3
	Output: [[1,2,3],[8,9,4],[7,6,5]]
	:param num:
	:return:
	"""
	#matrix = [[0 for i in range(3)] for j in range(3)]
	#创建值都是0的 n*n matrix
	matrix = list()
	for i in range(num):
		matrix.append([])
		for j in range(num):
			matrix[i].append(0)

	# 设置指针
	row_direction = [0, 1, 0, -1]
	col_direction = [1, 0, -1, 0]
	row = 0
	col = 0
	di = 0
	count = 0

	for i in range(1, num**2+1):
		# 判断是否超出范围或已经有非0的数值填入
		if row >= 0 and row < num and col >= 0 and col < num and matrix[row][col] == 0:
			matrix[row][col] = i
			row += row_direction[di]
			col += col_direction[di]
		else:
			#如果超出范围或已经有非0的数值填入，回退到上一个点
			row -= row_direction[di]
			col -= col_direction[di]
			#移动到正确的点
			count += 1
			di = count % 4
			row += row_direction[di]
			col += col_direction[di]
			#填入数值
			matrix[row][col] = i
			#计算下一个位置
			row += row_direction[di]
			col += col_direction[di]
	return matrix

print(generateMatrix(5))
