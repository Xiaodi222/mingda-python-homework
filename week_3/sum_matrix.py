

def sum_matrix(input):
    """
    输入参数是一个二维数组，返回结果也是一个二维数组。要求返回数组的每个位置的数字，是输入数组相同位置数字加上其左边所有数字，上边所有数字，
    左上所有数字的总和
    例子：
    输入为：
    [
    [1,2,3],
    [4,5,6],
    [7,8,9]
    ]
    则输出为
    [
    [1,3,6],
    [5,12,21],
    [12,27,45]
    ]
    以其中两个数字为例解释输出结果：
    12 = 1+2+4+4
    27 = 1+2+4+5+7+8
    :param input:
    :return:
    """
    # listv = []
    # for a in range(len(input)):
    #     listv.append([])
    #     for b in range(len(input[a])):
    #         v = 0
    #         for m in range(len(input)):
    #             for n in range(len(input[m])):
    #                 if m <= a and n <= b:
    #                     v += input[m][n]
    #         listv[a].append(v)
    # return listv


    listv = []
    for row in range(len(input)):
        listv.append([])
        for col in range(len(input[row])):
            if row == 0 and col ==0:
                num = input[row][col]
            elif row == 0:
                num = input[row][col] + listv[row][col-1]
            elif col == 0:
                num = input[row][col] + listv[row-1][col]
            else:
                num = input[row][col] + listv[row-1][col] + listv[row][col-1] - listv[row-1][col-1]
            listv[row].append(num)
    return listv


print(sum_matrix([
    [1,2,3,4],
    [4,5,6],
    [7,8,9]
    ]))