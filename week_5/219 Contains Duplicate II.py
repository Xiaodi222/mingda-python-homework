'''
Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.



Example 1:

Input: nums = [1,2,3,1], k = 3
Output: true
Example 2:

Input: nums = [1,0,1,1], k = 1
Output: true
Example 3:

Input: nums = [1,2,3,1,2,3], k = 2
Output: false



'''
# def containsNearbyDuplicate(nums, k):
#     for i in range(len(nums)):
#         for j in range(i+1,len(nums)):
#             if nums[i] == nums[j]:
#                 distance = abs(j - i)
#                 if distance <= k:
#                     return True
#     return False

def containsNearbyDuplicate(nums, k):
    nums_dict = dict()
    for i in range(len(nums)):
        if nums[i] in nums_dict and i - nums_dict[nums[i]] <= k:
            return True
        nums_dict[nums[i]] = i
    return False









nums = [1,2,3,1]
k = 3
print(containsNearbyDuplicate(nums, k))
nums = [1,0,1,1]
k = 1
print(containsNearbyDuplicate(nums, k))

nums = [1,2,3,1,2,3]
k = 2
print(containsNearbyDuplicate(nums, k))
