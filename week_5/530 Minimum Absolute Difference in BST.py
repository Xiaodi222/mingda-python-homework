'''
Given the root of a Binary Search Tree (BST), return the minimum absolute difference between the values of any two different nodes in the tree.



Example 1:


Input: root = [4,2,6,1,3]
Output: 1
Example 2:


Input: root = [1,0,48,null,null,12,49]
Output: 1
'''

#
# def getMinimumDifference(root):
#     current = TreeNode
#     leftnode = 0
#     rightnode = 0
#     rootnode = 0
#     if current.left is not None:
#         leftnode = current.left.getMinimumDifference()
#
#     if current.left is not None:
#         rightnode = current.left.getMinimumDifference()
#
#     if current.left is not None:
#         rootnode = current.val
#     return leftnode + rootnode + rightnode
#
#

 # current = root
 #        result = list()
 #        if current.left is not None:
 #            result.append(current.left.val)
 #            self.getMinimumDifference(current.left)
 #        if current.right is not None:
 #            result.append(current.right.val)
 #            self.getMinimumDifference(current.right)
 #        if current is not None:
 #            result.append(current.val)
 #        return result


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
# class Solution:
#     def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
#         a = sorted(self.getnode(root))
#         min_num = int(a[1]) - int(a[0])
#         for i in range(1, len(a)):
#             if int(a[i]) - int(a[i - 1]) < min_num:
#                 min_num = int(a[i]) - int(a[i - 1])
#         return min_num
#
#     def getnode(self, root):
#         current = root
#         leftnode = ""
#         rightnode = ""
#         rootnode = ""
#         if current.left is not None:
#             leftnode = self.getnode(current.left)
#         if current.right is not None:
#             rightnode = self.getnode(current.right)
#         if current.val is not None:
#             rootnode = str(current.val)
#         return leftnode + rootnode + rightnode
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def __init__(self):
        self.result = []

    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        nodes = sorted(self.getnode(root))
        min_num = abs(int(nodes[0]) - int(nodes[1]))
        for i in range(1, len(nodes)):
            if abs(int(nodes[i]) - int(nodes[i - 1])) < min_num:
                min_num = abs(int(nodes[i]) - int(nodes[i - 1]))
        return min_num

    def getnode(self, root):
        result = self.result
        current = root
        if current.left is not None:
            self.getnode(current.left)
        if current.right is not None:
            self.getnode(current.right)
        if current.val is not None:
            result.append(current.val)
        return result
