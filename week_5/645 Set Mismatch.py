'''You have a set of integers s, which originally contains all the numbers from 1 to n. Unfortunately, due to some error, one of the numbers in s got duplicated to another number in the set, which results in repetition of one number and loss of another number.

You are given an integer array nums representing the data status of this set after the error.

Find the number that occurs twice and the number that is missing and return them in the form of an array.



Example 1:

Input: nums = [1,2,2,4]
Output: [2,3]
Example 2:

Input: nums = [1,1]
Output: [1,2]
'''

#
# def findErrorNums(nums):
#     n = len(nums)
#     newlist = list()
#     result = list()
#     for i in range(1, n+1):
#         if i not in nums:
#             missing = i
#             break
#     for i in nums:
#         if i in newlist:
#             duplicate = i
#             break
#         else:
#             newlist.append(i)
#     result.append(duplicate)
#     result.append(missing)
#     return result



def findErrorNums(nums):
    n = len(nums)
    duplicate = sum(nums) - sum(set(nums))
    missing = int((1+n)*n/2) - sum(nums) + duplicate
    return [duplicate, missing]



print(findErrorNums([1, 2, 2, 4]))

