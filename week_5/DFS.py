
map = dict()
map['s'] = {'a', 'c'}
map['a'] = {'b', 'e'}
map['b'] = {'c', 'd', 'g1'}
map['c'] = {'d', 'g2'}
map['d'] = {'g1', 'g2'}
map['e'] = {'g1', 'g2'}
map['g1'] = {}
map['g2'] = {}




class Stack:
    def __init__(self):
        self.stack = list()

    def add(self, x):
        self.stack.append(x)

    def stack_pop(self):
        return self.stack.pop(-1)

    def get_length(self):
        return len(self.stack)

    def peek(self):
        return self.stack[-1]


class MapNode:
    def __init__(self, parent, name):
        self.parent = parent
        self.name = name

def DFS(Map):
    stack = Stack()
    closed = set()
    root_node = MapNode(None, 's')
    stack.add(root_node)
    closed.add(root_node.name)
    while stack.get_length() != 0:
        current_node = stack.peek()
        if current_node.name == 'g1' or current_node.name == 'g2':
            return find_path(current_node)
        successors = Map[current_node.name]
        for successor in successors:
            successor_node = MapNode(current_node, successor)
            if successor not in closed:
                stack.add(successor_node)
                closed.add(successor)

def find_path(goal_node):
    result = []
    while goal_node != None:
        result.append(goal_node.name)
        goal_node = goal_node.parent
    result.reverse()
    return result


a = Stack()
print(a.get_length())

