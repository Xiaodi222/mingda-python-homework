

map = dict()
map['s'] = {'a', 'c'}
map['a'] = {'b', 'e'}
map['b'] = {'c', 'd', 'g1'}
map['c'] = {'d', 'g2'}
map['d'] = {'g1', 'g2'}
map['e'] = {'g1', 'g2'}
map['g1'] = {}
map['g2'] = {}



class Queue:
    def __init__(self):
        self.queue = list()

    def enqueue(self,x):
        self.queue.append(x)

    def dequeue(self):
        self.queue.remove(self.queue[0])

    def peek(self):
        return self.queue[0]

    def get_len(self):
        return len(self.queue)

class MapNode:
    def __init__(self,parent,name):
        self.parent = parent
        self.name = name

def BFS(Map):
    queue = Queue()
    closed = set()
    root = MapNode(None, "s")
    queue.enqueue(root)
    closed.add(root.name)


    while queue.get_len() != 0:
        current = queue.peek()
        queue.dequeue()
        if current.name == "g1" or current.name == "g2":
            x = findpath(current, [])
            x.reverse()
            return x
        else:
            successors = Map[current.name]
            for successor in successors:
                SucNode = MapNode(current,successor)
                if successor not in closed:
                    queue.enqueue(SucNode)
                    closed.add(SucNode.name)

def findpath(current, result):
    while current is not None:
        result.append(current.name)
        current = current.parent
    return result

print(BFS(map))