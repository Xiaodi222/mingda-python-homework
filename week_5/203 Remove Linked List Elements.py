class Solution:
    def removeElements(self, head: Optional[ListNode], val: int) -> Optional[ListNode]:
        parent = ListNode(None, head)
        current = parent.next
        while current is not None:
            if current.val == val:
                current = current.next
                parent.next = current
            else:
                parent = current
                current = current.next
        return head

# [7,7,7,7]
# return [7,7,7,7] rather than []

'''
Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.

 

Example 1:


Input: head = [1,2,6,3,4,5,6], val = 6
Output: [1,2,3,4,5]
Example 2:

Input: head = [], val = 1
Output: []
Example 3:

Input: head = [7,7,7,7], val = 7
Output: []
'''
class Solution:
    def removeElements(self, head: Optional[ListNode], val: int) -> Optional[ListNode]:
        origin = ListNode(None, head)
        current = origin.next
        parent = origin
        while current is not None:
            if current.val == val:
                current = current.next
                parent.next = current
            else:
                parent = current
                current = current.next
        return origin.next