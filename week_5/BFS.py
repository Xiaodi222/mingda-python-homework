
map = dict()
map['s'] = {'a', 'c'}
map['a'] = {'b', 'e'}
map['b'] = {'c', 'd', 'g1'}
map['c'] = {'d', 'g2'}
map['d'] = {'g1', 'g2'}
map['e'] = {'g1', 'g2'}
map['g1'] = {}
map['g2'] = {}

class Queue:
    def __init__(self):
        self.queue = list()

    def enqueue(self, x):
        self.queue.append(x)

    def dequeue(self):
        self.queue.remove(self.queue[0])

    def peek(self):
        return self.queue[0]

    def get_length(self):
        return len(self.queue)

class MapNode:
    def __init__(self, parent, name):
        self.parent = parent
        self.name = name

def BFS(Map):
    queue = Queue()
    closed = set()
    root_node = MapNode(None, 's')
    queue.enqueue(root_node)
    closed.add(root_node.name)
    while queue.get_length() != 0:
        current_node = queue.peek()
        queue.dequeue()
        if current_node.name == 'g1' or current_node.name == 'g2':
            return find_path(current_node)
        successors = Map[current_node.name]
        for successor in successors:
            successor_node = MapNode(current_node, successor)
            if successor not in closed:
                queue.enqueue(successor_node)
                closed.add(successor)

def find_path(goal_node):
    result = []
    while goal_node != None:
        result.append(goal_node.name)
        goal_node = goal_node.parent
    result.reverse()
    return result

print(BFS(map))