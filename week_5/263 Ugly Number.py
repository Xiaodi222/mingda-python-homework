'''
An ugly number is a positive integer whose prime factors are limited to 2, 3, and 5.

Given an integer n, return true if n is an ugly number.



Example 1:

Input: n = 6
Output: true
Explanation: 6 = 2 × 3
Example 2:

Input: n = 1
Output: true
Explanation: 1 has no prime factors, therefore all of its prime factors are limited to 2, 3, and 5.
Example 3:

Input: n = 14
Output: false
Explanation: 14 is not ugly since it includes the prime factor 7.
'''
#
# def isUgly(n):
#     if n <= 1:
#         return False
#     prime_numbers = prime(n)
#     if 2 in prime_numbers:
#         prime_numbers.remove(2)
#     if 3 in prime_numbers:
#         prime_numbers.remove(3)
#     if 5 in prime_numbers:
#         prime_numbers.remove(5)
#
#     if len(prime_numbers) == 0:
#         return True
#     else:
#         return False
#
# def prime(n: int):
#     number = 2
#     prime = set()
#     while n != 1:
#         if n % number == 0:
#             prime.add(number)
#             n = n / number
#         else:
#             number += 1
#     return prime



#
# class Solution:
#     def isUgly(self, n: int) -> bool:
        # if n <= 1:
        #     return False
#         number = 2
#         prime_numbers = set()
#         while n != 1:
#             if n % number == 0:
#                 prime_numbers.add(number)
#                 n = n / number
#             else:
#                 number += 1
#
#         if 2 in prime_numbers:
#             prime_numbers.remove(2)
#         if 3 in prime_numbers:
#             prime_numbers.remove(3)
#         if 5 in prime_numbers:
#             prime_numbers.remove(5)
#
#         if len(prime_numbers) == 0:
#             return True
#         else:
#             return False
#


def isUgly(n):
    if n < 1:
        return False
    while n != 1:
        if n % 2 == 0:
            n = n / 2
        elif n % 3 == 0:
            n = n / 3
        elif n % 5 == 0:
            n = n / 5
        else:
            return False
    return True


print(isUgly(5))