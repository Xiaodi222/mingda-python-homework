'''
Given the root of a binary search tree (BST) with duplicates, return all the mode(s) (i.e., the most frequently occurred element) in it.

If the tree has more than one mode, return them in any order.

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than or equal to the node's key.
The right subtree of a node contains only nodes with keys greater than or equal to the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:


Input: root = [1,null,2,2]
Output: [2]
Example 2:

Input: root = [0]
Output: [0]
'''


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def __init__(self):
        self.result = []

    def findMode(self, root: Optional[TreeNode]) -> List[int]:
        nodes = sorted(self.getnode(root))
        frequent = list()
        nodes_dict = dict()
        print(nodes)
        for node in nodes:
            if node not in nodes_dict.keys():
                nodes_dict[node] = 1
            else:
                nodes_dict[node] += 1

        # MaxKey = max(nodes_dict, key=nodes_dict.get)
        max_value = max(nodes_dict.values())
        for key, value in nodes_dict.items():
            if value == max_value and key not in frequent:
                frequent.append(key)
        return frequent

    def getnode(self, root):
        result = self.result
        current = root
        if current.left is not None:
            self.getnode(current.left)
        if current.right is not None:
            self.getnode(current.right)
        if current.val is not None:
            result.append(current.val)
        return result