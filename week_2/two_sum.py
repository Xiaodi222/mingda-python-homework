

def two_sum(nums, target):
    """
    nums是一个数组，里面至少有2个整数。target是一个整数数字。要求从数组中找出一对数字，使其相加的和正好等于target。
    返回值对应数字的index，并储存在list中
    此题假设nums中有且只有一对数字之和正好等于target
    例子：
    nums = [2，7，11，15]
    target = 9
    则输出应为：[0,1]
    :param nums:
    :param target:
    :return:
    """
    for i in range(len(nums)-1):
        print("i",i)
        for n in range(i+1,len(nums)):
            print("n",n)
            if nums[i] + nums[n] == target:
                return [i,n]

nums = [2, 7, 11, 15]
target = 26
print(two_sum(nums, target))