

def count_characters1(input):
    """
    输入一个字符串，统计最多的那个符号，并返回该符号。
    此题假设有且只有一个符号最多
    例如，输入字符串"abcddefffff88512!!%3}", 则应返回"f"
    :param input:
    :return:
    """
    dict = {}
    for i in input:
        if i not in dict.keys():
            dict[i] = 1
        elif i in dict.keys():
            dict[i] += 1
    v = 0
    for i in dict.keys():
        if dict[i] > v:
            v = dict[i]
            k = i
    return k


print(count_characters1("abcddefffff88512!!%3}"))