

def palindrome(input):
    """
    输入是一个数字（可为负数），要求判断其是否为回文数字。
    121为回文数字，但是-121则不是
    回文数字返回True，否则返回False
    :param input:
    :return:
    """
    reverse_input = input[::-1]
    for i in range(len(input)):
        if input[i] != reverse_input[i]:
            return False
    return True

print(palindrome('abcba'))