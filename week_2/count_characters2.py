

def count_characters2(input):
    """
    输入一个字符串，统计总共含有多少种不同类型的符号，并返回类型个数
    例如，数字字符串"abcc"，则返回3
    :param input:
    :return:
    """
    a = []
    count = 0
    for i in input:
        if i not in a:
            a.append(i)
            count += 1
    return count

print(count_characters2("abccd"))