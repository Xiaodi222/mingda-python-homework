

def valid_parentheses(input):
    """
    输入是一串各种括号的字符串，包括小括号，中括号，大括号。括号使用规则与数学括号相同，即必须在正确的位置成对出现
    比如()就是正确格式，[()]也是正确格式
    但是[(])就不属于正确格式
    该函数要求判断输入的符号是否是正确格式的，正确返回True，否则返回False。
    :param input:
    :return:
    """
    a = []
    dict = {")":"(","]":"[","}":"{"}
    left = ["(","[","{"]
    right = [")","]","}"]
    for i in input:
        if i in left:
            a.append(i)
            print(1,a)
        elif i in right:
            if a.pop(-1) != dict[i]:
                print(a,dict[i])
                return False
    return True

print(valid_parentheses("[()]"))