

def count_characters3(input):
    """
    输入一个字符串list，找到同时出现在所有元素中的符号（重复的也需要重复计算），并返回重复的元素
    例如，输入的是[aaabbbccc,aabbcc,aabb]，则返回aabb。因为三个字符串中都出现了aabb
    :param input:
    :return:
    """
    for i in input:
        for t in i:
            if t in