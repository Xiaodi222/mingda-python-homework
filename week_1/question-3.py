

# def movie(hour, minute):
#
#
#     if hour >=2 and minute >=2:
#         return str(hour)+" hours"+" and "+ str(minute)+" minutes"
#     elif hour >=2 and minute >=1:
#         return str(hour)+" hours"+" and "+ str(minute)+ " minute"
#     elif hour >=2 and minute ==0:
#         return str(hour)+" hours"
#     elif hour ==1 and minute >=2:
#         return str(hour)+" hour"+" and "+str(minute)+" minutes"
#     elif hour ==1 and minute ==1:
#         return str(hour)+" hour"+" and "+str(minute)+" minute"
#     elif hour ==1 and minute ==0:
#         return str(hour)+" hours"
#     elif hour ==0 and minute >=2:
#         return str(minute)+" minutes"
#     elif hour ==0 and minute ==1:
#         return str(minute)+" minute"



def movie(hour, minute):
    """
    将输入的是电影播放时间（x小时，y分钟），需要将其转换为字符串格式输出。如果电影的播放时间大于1小时，则输出格式为"x hour(s) and y minute(s)"
    注意，只有数字为1的时候才带"s"。如果电影播放时间正好为整数小时，0分钟，则输出格式为"x hour(s)"，如果电影播放时间小于一个小时，则输出格式为
    "y minute(s)"。
    :param hour: 电影播放小时数，整数
    :param minute: 电影播放分钟数，整数
    :return: 电影播放时间的字符串
    """
    # TODO: 将电影播放时间输出为指定的字符串格式，并通过所有测试
    hourstring = ''
    if hour == 0:
        hourstring = ""
    elif hour == 1:
        hourstring = "1 hour"
    elif hour >= 2:
        hourstring = str(hour) + " hours"

    minutestring = ''
    if minute == 0:
        minutestring = ""
    elif minute == 1:
        minutestring = "1 minute"
    elif minute >= 2:
        minutestring = str(minute) + " minutes"

    if hour >= 1 and minute >= 1:
        hmand = " and "
    else:
        hmand = ""


    return hourstring + hmand + minutestring


def test():
    assert movie(0, 1) == '1 minute'
    assert movie(0, 30) == '30 minutes'
    assert movie(1, 0) == '1 hours'
    assert movie(1, 30) == '1 hour and 30 minutes'
    assert movie(1, 1) == '1 hour and 1 minute'
    assert movie(2, 1) == '2 hours and 1 minute'
    assert movie(2, 30) == '2 hours and 30 minutes'
    print("all tests passed")

test()