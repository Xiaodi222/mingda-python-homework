
#
# def prime_factor(input):
#     d = 0
#     for i in range(2,input+1):
#         for n in range(2, i+1):
#             print("n",n)
#             m = 0
#             if i % n == 0:
#                 m += 1
#                 print(m)
#                 break
#         if m != 1 and input % i == 0:
#             input = input/i
#             d += 1
#             print("P",input)
#
#     return d
#


def prime(input):
    if input == 2:
        return True
    for n in range(2, input):
        if input % n == 0:
            return False
    return True



def factor(input1, input2):
    if input2 % input1 == 0:
        return True
    return False


def prime_factor(input):
    count = 0
    for i in range(2, input + 1):
        if prime(i) and factor(i, input):
            count += 1
    return count











    # 输入一个正整数n，输出n的质因数的个数
    # :param input: 正整数
    # :return: 质因数的个数
    # TODO: 输入一个正整数n，输出n的质因数的个数，并通过所有测试


# print(prime_factor(120))



def test():
    assert prime_factor(1) == 0
    assert prime_factor(2) == 1
    assert prime_factor(3) == 1
    assert prime_factor(10) == 2
    assert prime_factor(12) == 2
    assert prime_factor(120) == 3
    assert prime_factor(2730) == 5
    print("all tests passed")

test()

