

def triangle(input):
    for i in range(1,input+1):
        a = []
        s = input+1-i
        for t in range(i):
            a.append(s+2*t)
        for n in range(1,2*input):
            if n in a:
                print("*",end =" ")
            else:
                print(" ",end =" ")
        print("")
triangle(5)




#
#
#     input * 2 - 1
#
#     def triangle(input):
#         a = []
#         for n in range(input * 2 - 1):
#             if n == input - 1:
#                 a.append("*")
#             else:
#                 a.append("_")
#         # print(a)
#         for p in a:
#             print(p),
#
#
#     triangle(3)
#
# input * 2 - 1
#
#     def triangle(input):
#         a = []
#         for n in range(input * 2 - 1):
#             if n == input - 1:
#                 a.append("*")
#             else:
#                 a.append("_")
#         # print(a)
#         for p in a:
#             print(p),
#
#       triangle(3)


"""
    输入正整数n，print对应行数n的三角形
    例如，输入数字3，则打印
    __*__
    _*_*_
    *_*_*
    如果输入数字4，则打印
    ___*___
    __*_*__
    _*_*_*_
    *_*_*_*
    提示：找到每行的规律后再开始写代码
    下划线_是print时应该有的空格，此处下划线取代是方便观察
    :param input: 三角形的行数，正整数
    :return: None
    """
    # TODO: 先找规律，再写代码。这一题不需要返回任何值，而是print出希望得到的三角形
