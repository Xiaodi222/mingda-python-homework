

def grade(input):
    if input
    """
    将input的分数转换为分数等级。0-49分为N，50-59分为P，60-69分为C，70-79分为D，80-100分为HD
    :param input: 整数分数
    :return: 分数等级字符串
    """
    # TODO: 将input的分数转换为分数等级，并通过所有测试
    return None

def test():
    assert grade(0) == 'N'
    assert grade(30) == 'N'
    assert grade(49) == 'N'
    assert grade(55) == 'P'
    assert grade(60) == 'C'
    assert grade(71) == 'D'
    assert grade(88) == 'HD'
    assert grade(100) == 'HD'
    print("all tests passed")

test()