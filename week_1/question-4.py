

def even_numbers(input):
    evennumber = []
    i = 1
    while i <=input:
        if i%2==0:
            evennumber.append(i)
        i=i+1
    return evennumber
    """
    输入的是一个整数n，找到介于1-n(包括n本身)之间所有的偶数，按照从小到大的顺序存在list中，并返回
    :param input: 正整数
    :return: 包含1-n中所有的偶数的list，且升序储存
    """
    # TODO: 找到1-n中所有的偶数，并通过所有测试

def test():
    assert even_numbers(1) == []
    assert even_numbers(2) == [2]
    assert even_numbers(6) == [2,4,6]
    assert even_numbers(10) == [2,4,6,8,10]
    assert even_numbers(11) == [2,4,6,8,10]
    print("all tests passed")

test()