


"""
    输入；两个只含有字母的字符串，如果这第一个字符串是第二个字符串的重排列，则返回True，否则返回False。重排列指的是第一个字符串中的字母以某种
    方式打乱顺序后形成的第二个字符串。例如，cats和acst就属于一对重排列；但是cats和catt都不属于cate的重排列
    :param input1: 只含有字母的字符串
    :param input2: 只含有字母的字符串
    :return: Boolean Value
    """

# def permute(input1, input2):
#     a = []
#     for n in input1:
#         a.append(n)
#     b = []
#     for n in input2:
#         b.append(n)
#     for i1 in range(len(a)):
#         for i2 in range(len(b)):
#             if a[i1] == b[i2]:
#                 b.pop(i2)
#                 break
#     if b==[]:
#         return True
#     else:
#         return False


def permute(input1, input2):
    dict1 = {}
    dict2 = {}
    for i in input1:
        if i not in dict1.keys():
            dict1[i] = 1
        elif i in dict1.keys():
            dict1[i] += 1


    for i in input2:
        if i not in dict2.keys():
            dict2[i] = 1
        elif i in dict2.keys():
            dict2[i] += 1

    print(dict1,dict2)

    if len(dict1) != len(dict2):
        return False
    else:
        for i in dict1.keys():
            if i not in dict2.keys():
                return False
            elif dict1[i] != dict2[i]:
                return False
        return True



def test():
    assert permute('a', 'a') == True
    assert permute('ab', 'ba') == True
    assert permute('a', 'b') == False
    assert permute('ab', 'an') == False
    assert permute('afh', 'axmz') == False
    assert permute('abc', 'abc') == True
    assert permute('hots', 'otsh') == True
    assert permute('abcdefgh', 'hcdgbeaf') == True
    assert permute('abhlwdbh', 'aqeib') == False
    print("all tests passed")

test()