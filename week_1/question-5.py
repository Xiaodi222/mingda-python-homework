


"""
    输入一个十进制的正整数，将其转换为二进制后，统计二进制数中包含多少个1。比如十进制5转换为二进制后为101，总共2个1，故返回结果为2
    :param input: 正整数
    :return: 二进制中1的个数
    """
    # TODO: 统计输入的数字转为二进制后1的个数，并通过所有测试


def ones(input):

    n = 0
    while True:
        quotient = input // 2
        remainder = input % 2
        input = quotient
        if remainder == 1:
            n += 1
        if input == 0:
            break
    return n



def test():
    assert ones(1) == 1
    assert ones(2) == 1
    assert ones(5) == 2
    assert ones(10) == 2
    assert ones(100) == 3
    assert ones(9999) == 8
    assert ones(12345678) == 12
    print("all tests passed")

test()