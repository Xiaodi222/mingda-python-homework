

def toUpperCases(input):
    """
    将input中所有的小写字母转换为大写字母，其他字母保持不变
    :param input: 含有各种字母的字符串
    :return: 不包含小写字母的字符串
    """
    output=""
    for i in range(len(input)):
        if ord(input[i]) > 96 and ord(input[i]) < 123:
            output += chr(ord(input[i])-32)
        else:
            output += input[i]
    return output

    # TODO: 将函数输入的字母全部变为大写，并通过所有测试

print((toUpperCases('aAA')))

def test():
    assert toUpperCases('a') == 'A'
    assert toUpperCases('A') == 'A'
    assert toUpperCases('aA') == 'AA'
    assert toUpperCases('Aa') == 'AA'
    assert toUpperCases('abcABc') == 'ABCABC'
    assert toUpperCases('abcDEf123!@#') == 'ABCDEF123!@#'
    print("all tests passed")

test()