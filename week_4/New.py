class LinkedList:
    value = None
    next = None

    def __init__(self):
        pass


    def add(self, num, index = None):
        NewLL = LinkedList()
        NewLL.value = num
        CurrentLL = self
        if CurrentLL.value is None:
            CurrentLL.value = num
        else:
            if index is None:
                while CurrentLL.next is not None:
                    CurrentLL = CurrentLL.next
                CurrentLL.next = NewLL
            else:
                temp = 1
                while temp != index:
                    CurrentLL = CurrentLL.next
                    temp += 1
                New_next = CurrentLL.next
                CurrentLL.next = NewLL
                NewLL.next = New_next


    def remove(self,num):
        CurrentLL = self
        PreLL = None
        while CurrentLL.value != num:
            PreLL = CurrentLL
            CurrentLL = CurrentLL.next
            if CurrentLL is None:
                return
        PreLL.next = CurrentLL.next


    def printLL(self):
        CurrentLL = self
        printed = ""
        while CurrentLL is not None:
            printed += str(CurrentLL.value) + " || "
            CurrentLL = CurrentLL.next

        return printed

#
# LL = LinkedList()
# LL.add(1)
# LL.add(3)
# LL.add(5)
# LL.add(7)

# print(LL.printLL())
#
# LL.remove(5)
# print(LL.printLL())
#
# LL.add(10,2)
# print(LL.printLL())




class Hashtable:
    maxlen = 8

    def __init__(self):
        self.table = list()
        self.initialize()

    def initialize(self):
        for i in range(self.maxlen):
            self.table.append(LinkedList())

    def gethash(self, num):
        return num % self.maxlen

    def inside(self, key):
        hashvalue = self.gethash(key)
        LL = self.table[hashvalue]
        while LL.value is not None:
            if LL.value.key == key:
                return True
            LL = LL.next
        return False


    def add(self, key, value):
        hashvalue = self.gethash(key)
        LL = self.table[hashvalue]
        if not self.inside(key):
            LL.add(kvp(key, value))

    def to_string(self):
        printed = "{"
        for LL in self.table:
            if LL.value is None:
                continue
            else:
                printed += str(LL.value.key) + ": " + str(LL.value.value) + ", "
        return printed[:-2] + "}"

class kvp:
    def __init__(self,key,value):
        self.key = key
        self.value = value

table = Hashtable()
table.add(1,2)
table.add(3,2)

print(table.to_string())



