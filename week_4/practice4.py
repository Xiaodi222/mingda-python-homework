from week_3.LinkedList import LinkedList

class HashTable:
    maxl = 8

    def __init__(self):
        self.table = list()
        self.initialize()

    def initialize(self):
        for i in range(self.maxl):
            self.table.append(LinkedList())

    def gethash(self,num):
        return num % self.maxl

    def inside(self,num):
        hashv = self.gethash(num)
        LL = self.table[hashv]
        while LL.value is not None:
            if LL.value.key == num:
                return True
            LL = LL.next
        return False

    def add(self,key,value):
        hashv = self.gethash(key)
        LL = self.table[hashv]
        if not self.inside(key):
            LL.add(kvp(key,value))


    def to_string(self):
        printed = "{"
        for LL in self.table:
            if LL.value is None:
                continue
            while LL is not None:
                printed += str(LL.value.key) +": " +str(LL.value.value) + ", "
                LL = LL.next
        printed = printed[:-2]
        printed += "}"

        return printed

class kvp:
    def __init__(self,key,value):
        self.key = key
        self.value = value


table = HashTable()
table.add(1,2)
table.add(4,3)


print(table.to_string())