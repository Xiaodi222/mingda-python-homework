from week_3.LinkedList import LinkedList


class HashTable:
    # 规定这个set只能储存英文字符串
    max_length = 8 # 坑的个数

    def __init__(self):
        self.table = list() # 初始化坑
        self.initialize()

    def initialize(self):
        for i in range(self.max_length):
            self.table.append(LinkedList())

    def get_hash_value(self, input):
        # 哈希函数
        return input ** 5 % 8
        # value = 0
        # for character in input:
        #     value += ord(character)
        # value = value % 8
        # return value

    def add(self, key, value):
        hash_value = self.get_hash_value(key)
        if not self.inside(key):
            kvpair = KVPair(key, value)
            self.table[hash_value].add(kvpair)


    def inside(self, key):
        hash_value = self.get_hash_value(key)
        LL = self.table[hash_value]
        # current_ll = LL
        while LL.value is not None:
            if LL.value.key == key:
                return True
            LL = LL.next
        return False

    def to_string(self):
        printed = "{"
        for LL in self.table:
            if LL.value is None:
                continue
            while LL is not None:
                printed += str(LL.value.key) +": " +str(LL.value.value) + ", "
                LL = LL.next
        printed = printed[:-2]
        printed += "}"

        return printed




class KVPair:
    def __init__(self, key, value):
        self.key = key
        self.value = value



table = HashTable()
table.add(1,2)
table.add(4,3)


print(table.to_string())
