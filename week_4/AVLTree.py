

class AVLTree:
    left = None
    right = None
    value = None

    def __init__(self, left, right, value):
        self.left = left
        self.right = right
        # self.parent = parent
        self.value = value

    def height(self):
        left_height = 1
        right_height = 1
        if self.left is not None:
            left_height = self.left.height() + 1
        if self.right is not None:
            right_height = self.right.height() + 1
        return max(left_height, right_height)

    def get_balance(self):
        """
        检查当前树是否左右平衡
        :return:
        """
        if self.left is None and self.right is None:
            return True
        elif self.left is None and self.right is not None:
            if self.right.height() == 2:
                return False
            else:
                return True
        elif self.right is None and self.left is not None:
            if self.left.height() == 2:
                return False
            else:
                return True
        else:
            if abs(self.left.height() - self.right.height()) > 1:
                return False
            else:
                return True

    def left_rotate(self):
        new_left = AVLTree(None, None, None)
        new_left.value = self.value
        new_left.left = self.left
        new_left.right = self.right.left
        new_right = self.right.right
        self.value = self.right.value
        self.left = new_left
        self.right = new_right

    def right_rotate(self):
        new_right = AVLTree(None, None, None)
        new_right.value = self.value
        new_right.left = self.left.right
        new_right.right = self.right
        new_left = self.left.left
        self.value = self.left.value
        self.left = new_left
        self.right = new_right

    def add(self, num):
        # step1: 比较大小，决定放的位置
        # step2: 将元素放入树中
        # step3: 判断树的平衡，如果不平衡，则旋转
        if self.value is None:
            self.value = num
        else:
            if num < self.value:
                if self.left is None:  # 如果当前的左节点是None
                    self.left = AVLTree(None, None, num)
                else:  # 如果当前的左节点已经被定义（已经有一个数字在左边了）， 递归的往左边添加数字
                    self.left.add(num)
                    # if not self.get_balance():

            elif num > self.value:
                if self.right is None:
                    self.right = AVLTree(None, None, num)
                else:
                    self.right.add(num)
                    # if not self.get_balance():
            else:
                return
        # print(num, self.height())

        if not self.get_balance():
            print('rotate')
            if self.left is None:
                left_h = 0
            else:
                left_h = self.left.height()
            if self.right is None:
                right_h = 0
            else:
                right_h = self.right.height()
            if left_h > right_h:
                if self.left.left is None:
                    ll_h = 0
                else:
                    ll_h = self.left.left.height()
                if self.left.right is None:
                    lr_h = 0
                else:
                    lr_h = self.left.right.height()
                if ll_h > lr_h:
                    print('case1')
                    self.right_rotate()
                elif ll_h < lr_h:
                    print('case2')
                    self.left.left_rotate()
                    self.right_rotate()
            elif left_h < right_h:
                if self.right.right is None:
                    rr_h = 0
                else:
                    rr_h = self.right.right.height()
                if self.right.left is None:
                    rl_h = 0
                else:
                    rl_h = self.right.left.height()
                if rr_h > rl_h:
                    print('case3')
                    self.left_rotate()
                elif rr_h < rl_h:
                    print('case4')
                    self.right.right_rotate()
                    self.left_rotate()

    def beautiful_print(self):
        left_string = 'None'
        right_string = 'None'
        self_string = 'None'

        if self.left is not None:
            left_string = self.left.beautiful_print()
        if self.right is not None:
            right_string = self.right.beautiful_print()
        if self.value is not None:
            self_string = str(self.value)
        return left_string + ', ' + self_string + ', ' + right_string

a = AVLTree(None, None, None)
a.add(14)
a.add(17)
a.add(11)
a.add(7)
a.add(53)
a.add(4)
a.add(13)
a.add(12)
a.add(8)
a.add(60)
a.add(19)
a.add(16)
a.add(20)

print(a.beautiful_print())


print(a.beautiful_print())
print('a value:', a.value)
print(a.right.value)
print(a.left.value)