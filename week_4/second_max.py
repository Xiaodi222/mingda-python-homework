

def second_max(input):
    """
    输入一个list，里面有一串整数，找出从大到小排第二的数字
    :param input:
    :return:
    """
    # new_list = [input[0]]
    # for num in range(1,len(input)):
    # # for num in input:
    #     print("num", input[num])
    #     print(new_list)
    #     for i in range(len(new_list)):
    #         print("i",i)
    #         if input[num] >= new_list[i]:
    #             new_list.insert(i,input[num])
    #             print("new_list",new_list)
    #             break
    #         else:
    #             new_list.append(input[num])
    #             break

    for i in range(len(input)):
        for j in range(i + 1, len(input)):
            if input[i] > input[j]:
                input[i], input[j] = input[j], input[i]
    return input[-2]

print(second_max([3,2,5,4,3,6,9]))

#for i in range(7,7): 并没有报错，而是结束循环



