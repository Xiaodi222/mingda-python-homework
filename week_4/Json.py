import json
import os

with open('reference.json', encoding='utf-8') as f:
    data = json.load(f)

images = data['_via_img_metadata']

for image, item in images.items():
    region = item["regions"]
    for area in region:
        if area["shape_attributes"]["name"] == "rect":
            x = area["shape_attributes"]["x"]
            y = area["shape_attributes"]["y"]
            w = area["shape_attributes"]["width"]
            h = area["shape_attributes"]["height"]
            point1x = x
            point2x = x
            point3x = x + w
            point4x = x + w
            point1y = y
            point2y = y + h
            point3y = y + h
            point4y = y
            area["shape_attributes"]["all_points_x"] = [point1x, point2x, point3x, point4x]
            area["shape_attributes"]["all_points_y"] = [point1y, point2y, point3y, point4y]
            area["shape_attributes"].pop("x")
            area["shape_attributes"].pop("y")
            area["shape_attributes"].pop("width")
            area["shape_attributes"].pop("height")
            area["shape_attributes"]["name"] = "polygon"


folder = os.path.exists("./json_images/")
if not folder:
    os.makedirs("./json_images/")
else:
    print("Exist")


for image, item in images.items():
    path = os.path.join('./json_images/' + image + '.json')
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(item, f, indent=2)



#
# with open('./a/reference_new.json', 'w', encoding='utf-8') as f:
#     json.dump(data, f, indent=2)


# with open('reference_new.json', 'w', encoding='utf-8') as f:
#     json.dump(data, f, indent=2)