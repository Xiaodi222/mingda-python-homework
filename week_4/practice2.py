class LinkedList:
    value = None
    next = None

    def __init__(self):
        pass

    def add(self,num,index = None):
        new = LinkedList()
        new.value = num
        current = self
        if self.value is None:
            self.value = num
        else:
            if index is None:
                while current.next is not None:
                    current = current.next
                current.next = new
            else:
                tem = 1
                while tem != index:
                    current = current.next
                    tem += 1
                new_next = current.next
                current.next = new
                new.next = new_next

    def inside(self, num):
        current = self
        while current.value != num:
            current = current.next
            if current is None:
                return False
        return True

    def printLL(self):
        current = self
        printed = ""
        while current is not None:
            printed += str(current.value) + "||"
            current = current.next
        return printed

    def remove(self,num):
        current = self
        parent = None
        if self.inside(num):
            while current.value != num:
                parent = current
                current = current.next
            parent.next = current.next

LL = LinkedList()
LL.add(5)
LL.add(1)
LL.add(2)
LL.add(4)
LL.add(7)
LL.add(9)
print(LL.printLL())
LL.add(10,4)
print(LL.printLL())
LL.remove(7)
print(LL.printLL())