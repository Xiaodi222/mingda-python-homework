class AVLTree:
    left = None
    right = None
    value = None

    def __init__(self,left,right,value):
        self.left = left
        self.right = right
        self.value = value

    def height(self):
        lh = 1
        rh = 1
        # if current is None:
        #     return 0
        if self.left is not None:
            lh = self.left.height() + 1
        if self.right is not None:
            rh = self.right.height() + 1
        return max(lh,rh)

    def balance(self):
        if self.left is None and self.right is None:
            return True
        elif self. left is not None and self.right is None:
            if self.left.height() > 1:
                return False
            else:
                return True
        elif self. left is None and self.right is not None:
            if self.right.height() > 1:
                return False
            else:
                return True
        else:
            if abs(self.left.height()-self.right.height())>1:
                return False
            else:
                return True

    def right_rotate(self):
        new_right = AVLTree(None,None,None)
        new_right.value = self.value
        new_right.right = self.right
        new_right.left = self.left.right
        new_left = self.left.left
        self.value = self.left.value
        self.right = new_right
        self.left = new_left

    def left_rotate(self):
        new_left = AVLTree(None,None,None,)
        new_left.value = self.value
        new_left.left = self.left
        new_left.right = self.right.left
        new_right = self.right.right
        self.value = self.right.value
        self.left = new_left
        self.right = new_right



    def add(self,num):
        if self.value is None:
            self.value = num
        elif num < self.value:
            if self.left is None:
                self.left = AVLTree(None,None,num)
            else:
                self.left.add(num)
        elif num > self.value:
            if self.right is None:
                self.right = AVLTree(None,None,num)
            else:
                self.right.add(num)
        else:
            return

        # if not self.balance():
        #     print(self.left.value,self.right)
        #     if self.left.height() > self.right.height():
        #         if self.left.left.height > self.left.right.height():
        #             self.right_rotate()
        #         else:
        #             self.left.left_rotate()
        #             self.right_rotate()
        #     if self.right.height() > self.left.height():
        #         if self.right.right.height > self.right.left.height():
        #             self.left_rotate()
        #         else:
        #             self.right.right_rotate()
        #             self.left_rotate()

        if not self.balance():
            if self.left is None:
                lh = 0
            else:
                lh = self.left.height()
            if self.right is None:
                rh = 0
            else:
                rh = self.right.height()
            if lh > rh:
                if self.left.left is None:
                    llh = 0
                else:
                    llh = self.left.left.height()
                if self.left.right is None:
                    lrh = 0
                else:
                    lrh = self.left.right.height()
                if llh > lrh:
                    self.right_rotate()
                else:
                    self.left.left_rotate()
                    self.right_rotate()


    def beautiful_print(self):
        left_string = 'None'
        right_string = 'None'
        self_string = 'None'

        if self.left is not None:
            left_string = self.left.beautiful_print()
        if self.right is not None:
            right_string = self.right.beautiful_print()
        if self.value is not None:
            self_string = str(self.value)
        return left_string + ', ' + self_string + ', ' + right_string


a = AVLTree(None, None, None)
a.add(14)
a.add(17)
a.add(11)
a.add(7)
a.add(53)
a.add(4)
# a.add(13)
# a.add(12)
# a.add(8)
# a.add(60)
# a.add(19)
# a.add(16)
# a.add(20)

print(a.beautiful_print())
print(a.height())
