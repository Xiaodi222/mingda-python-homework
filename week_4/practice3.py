from week_3.LinkedList import LinkedList

class HashSet:
    maxl = 8

    def __init__(self):
        self.table = list()
        self.initialize()

    def initialize(self):
        for i in range(self.maxl):
            self.table.append(LinkedList())

    def gethash(self,num):
        return num % 8

    def add(self,num):
        hashv = self.gethash(num)
        LL = self.table[hashv]
        if not LL.inside(num):
            self.table[hashv].add(num)

    def remove(self,num):
        hashv = self.gethash(num)
        LL = self.table[hashv]
        if LL.inside(num):
            self.table[hashv].remove(num)

    def printH(self):
        printed = "{"
        for LL in self.table:
            if LL.value is None:
                continue
            else:
                NewLL = LL
                while NewLL is not None:
                    printed += str(NewLL.value) + ", "
                    NewLL = NewLL.next
        printed = printed[:-2]
        printed += "}"
        return printed


num_set = HashSet()
num_set.add(2)
num_set.add(10)
num_set.add(1)
num_set.add(5)
num_set.add(8)
print(num_set.printH())

