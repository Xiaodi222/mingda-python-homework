def groupAnagrams(input):
    """
    题目在https://leetcode.com/problems/group-anagrams/
    可以直接在网站上写代码测试代码正确性
    :param input:
    :return:
    """
    dict_anagram = {}
    anagram_list = []
    values_set = set()
    i = 0
    for word in input:
        sorted_word = "".join(sorted(word))
        dict_anagram[word] = sorted_word
        values_set.add(sorted_word)
    for values in values_set:
        anagram_list.append([])
        for keys in dict_anagram.keys():
            if dict_anagram[keys] == values:
                anagram_list[i].append(keys)
        i += 1
    return anagram_list

#
#
# def order(input):
#     new_list = [input[0]]
#     for num in range(1,len(input)):
#         for i in range(len(new_list)-1,-1,-1):
#             if ord(input[num]) >= ord(new_list[i]):
#                 new_list.append(input[num])
#                 break
#             else:
#                 new_list.append(input[num])
#                 break
#     return new_list

strs = ["eat","tea","tan","ate","nat","bat"]
# # print(order("ate"))
# # print(dict_anagram)
print(groupAnagrams(strs))