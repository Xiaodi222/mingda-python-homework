

def magic(num):
    res = 0
    if num % 2 == 0:
        if 0 < num < 100:
            res = num * 2
        else:
            res = num + 2
    else:
        if num < 0:
            res = num * (-1)
        elif num > 100:
            res = num - 100
        else:
            res = 1
    return res