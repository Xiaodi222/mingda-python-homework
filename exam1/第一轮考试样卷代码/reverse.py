import unittest

def reverse(input):
    """
    输入一个字符串，将其反序输出
    :param input:
    :return:
    """
    rev_str = ""
    for i in range(len(input) - 1, -1, -1):
        rev_str += input[i]
    return rev_str


class Test(unittest.TestCase):

    def test_empty(self):
        self.assertEqual('', reverse(''))

    def test_single(self):
        self.assertEqual('a', reverse('a'))
        self.assertEqual('1', reverse('1'))
        self.assertEqual('.', reverse('.'))

    def test_simple(self):
        self.assertEqual('cba', reverse('abc'))
        self.assertEqual('zyx', reverse('xyz'))
        self.assertEqual('@!.', reverse('.!@'))

    def test_complex1(self):
        self.assertEqual('./@j90kc.sa', reverse('as.ck09j@/.'))
        self.assertEqual('423ijp/.[]', reverse('][./pji324'))

    def test_complex2(self):
        self.assertEqual('#T@&*#@[,431p0ufaen9p;  e930', reverse('039e  ;p9neafu0p134,[@#*&@T#'))


if __name__ == '__main__':
    unittest.main()
