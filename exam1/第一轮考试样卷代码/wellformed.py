import unittest

def wellformed(input, key):
    # TODO: 超过90则自动变为32，即所有字符只从下面列表中出现
    return None

"""
      ASCII Character codes (for information)

      32  SPACE     64  @
      33  !         65  A
      34  "         66  B
      35  #         67  C
      36  $         68  D
      37  %         69  E
      38  &         70  F
      39  '         71  G
      40  (         72  H
      41  )         73  I
      42  *         74  J
      43  +         75  K
      44  ,         76  L
      45  -         77  M
      46  .         78  N
      47  /         79  O
      48  0         80  P
      49  1         81  Q
      50  2         82  R
      51  3         83  S
      52  4         84  T
      53  5         85  U
      54  6         86  V
      55  7         87  W
      56  8         88  X
      57  9         89  Y
      58  :         90  Z
      59  ;
      60  <
      61  =
      62  >
      63  ?
"""

class Test(unittest.TestCase):

    def test1(self):
        self.assertEqual(")UNK%\"KQ:8;E!+Z>ZQQ8:.R%(9@3#", wellformed("'RJFZVCH0-/8NWJ-H>=#$R:GIY$Q@", 2))