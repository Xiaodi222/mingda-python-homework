import unittest


def factorial(n):
    """
    计算n的阶乘，即1*2*...*n
    :param n:
    :return:
    """
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


class Test(unittest.TestCase):

    def test_one(self):
        self.assertEqual(factorial(1), 1)

    def test_two(self):
        self.assertEqual(factorial(2), 2)

    def test_three(self):
        self.assertEqual(factorial(3), 6)

    def test_ten(self):
        self.assertEqual(factorial(10), 3628800)

    def test_twenty(self):
        self.assertEqual(factorial(20), 2432902008176640000)