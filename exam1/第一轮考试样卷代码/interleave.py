import unittest

def interleave(a, b):
    """
     * This function takes two lists of strings and returns a new list of strings where
     * the elements from a and be are interleaved.
     * For example:
     *    a = ["a", "b", "c", "d"]
     *    b = ["e", "f", "g", "h"]
     * the result will be
     *        ["a", "e", "b", "f", "c", "g", "d", "h"]
     * in the case where one list is longer than the other, the remaining elements
     * will be added to the end of the result:
     *    a = ["a", "b", "c"]
     *    b = ["e"]
     * the result will be
     *        ["a", "e", "b", "c"]
    :param a:
    :param b:
    :return:
    """
    new_list = list()
    list_len = min(len(a), len(b))
    for i in range(list_len):
        new_list.append(a[i])
        new_list.append(b[i])
    if len(a) < len(b):
        for i in range(list_len, len(b)):
            new_list.append(b[i])
    elif len(a) > len(b):
        for i in range(list_len, len(a)):
            new_list.append(a[i])
    return new_list

class Test(unittest.TestCase):

    def test_empty(self):
        self.assertEqual(interleave([],[]), [])

    def test_simgle(self):
        self.assertEqual([1, 2, 3], interleave([1, 2, 3], []))
        self.assertEqual(['a', 'b', 'c'], interleave(['a', 'b', 'c'], []))
        self.assertEqual([1, 2, 3], interleave([], [1, 2, 3]))
        self.assertEqual(['a', 'b', 'c'], interleave([], ['a', 'b', 'c']))

    def test_equal_length(self):
        self.assertEqual([1, 1, 2, 2, 3, 3], interleave([1, 2, 3], [1, 2, 3]))
        self.assertEqual(['a', 'a', 'b', 'b', 'c', 'c'], interleave(['a', 'b', 'c'], ['a', 'b', 'c']))

    def test_different_length1(self):
        self.assertEqual(["a", "e", "b", "c"], interleave(["a", "b", "c"], ['e']))
        self.assertEqual([9, 1, 8, 2, 7, 6], interleave([9, 8, 7, 6], [1, 2]))

    def test_different_length2(self):
        self.assertEqual(["aa", "dd", "bb", "ee", 'cc', 'ff', 'gg'], interleave(['aa', 'bb', 'cc'], ['dd', 'ee', 'ff', 'gg']))
        self.assertEqual([9, 1, 8, 2, 100, 1000], interleave([9, 8], [1, 2, 100, 1000]))

if __name__ == '__main__':
    unittest.main()