import unittest


class Book:
    def __init__(self, name, year, numbers, level=None):
        # TODO: 修改下面代码，使参数初始化。status需调用initialize_status函数初始化
        self.name = None
        self.year = None
        self.numbers = None
        self.level = None
        self.status = None

    def initialize_status(self):
        status = {'in-library': self.numbers, 'outside-library': 0, 'lost': 0}
        return status

    def borrow_book(self):
        # TODO: 借出图书，in-library少一本，outside-library多一本。in-library不够时，需raise exception：No book is in the library
        pass

    def return_book(self):
        # TODO: 归还图书，in-library多一本，outside-library少一本。outside-library不够时，需raise exception：No book is outside
        pass

    def lost_book(self):
        # TODO: 丢失图书，in-library少一本，lost多一本。in-library不够时，需raise exception：No book is in the library
        pass


class LawBook(Book):
    def check_over_20_years(self):
        # TODO: 年份与2022年做比较，超过（不含）20年返回True，否则False
        return False

    def get_info(self):
        # TODO: 返回这本法律图书的信息，信息格式为 Law Book: 书名, in-library: x, outside-library: y, lost: z
        return None

class MathBook(Book):
    def check_level(self):
        # TODO: 对数学图书登记分类，大于等于0且小于等于10，返回primary student's book，大于10但小于等于20，返回secondary student's book，大于20但小于等于30，返回high school student's book，大于30但小于等于50返回normal book。其他情况raise exception：Wrong level
        return None

    def get_info(self):
        # TODO: 返回这本数学图书的信息，信息格式为 Math Book: 书名, level: n, in-library: x, outside-library: y, lost: z
        return None

class Test(unittest.TestCase):

    def test_initial_book_status(self):
        book = Book('Love', 1989, 5)
        self.assertEqual({'in-library': 5, 'outside-library': 0, 'lost': 0}, book.status)

    def test_borrow_book_status1(self):
        book = Book('Love', 1989, 0)
        with self.assertRaises(Exception) as e:
            book.borrow_book()
        self.assertTrue('No book is in the library' in str(e.exception))

    def test_borrow_book_status2(self):
        book = Book('Love', 1989, 5)
        book.borrow_book()
        self.assertEqual({'in-library': 4, 'outside-library': 1, 'lost': 0}, book.status)
        book.borrow_book()
        self.assertEqual({'in-library': 3, 'outside-library': 2, 'lost': 0}, book.status)
        book.borrow_book()
        self.assertEqual({'in-library': 2, 'outside-library': 3, 'lost': 0}, book.status)

    def test_return_book_status1(self):
        book = Book('Love', 1989, 5)
        book.borrow_book()
        book.borrow_book()
        book.borrow_book()
        book.return_book()
        self.assertEqual({'in-library': 3, 'outside-library': 2, 'lost': 0}, book.status)
        book.return_book()
        self.assertEqual({'in-library': 4, 'outside-library': 1, 'lost': 0}, book.status)
        book.return_book()
        self.assertEqual({'in-library': 5, 'outside-library': 0, 'lost': 0}, book.status)

    def test_return_book_status2(self):
        book = Book('Love', 1989, 5)
        with self.assertRaises(Exception) as e:
            book.return_book()
        self.assertTrue('No book is outside' in str(e.exception))

    def test_lost_book_status1(self):
        book = Book('Love', 1989, 0)
        with self.assertRaises(Exception) as e:
            book.lost_book()
        self.assertTrue('No book is in the library' in str(e.exception))

    def test_lost_book_status2(self):
        book = Book('Love', 1989, 5)
        book.lost_book()
        self.assertEqual({'in-library': 4, 'outside-library': 0, 'lost': 1}, book.status)
        book.lost_book()
        self.assertEqual({'in-library': 3, 'outside-library': 0, 'lost': 2}, book.status)

    def test_law_book_year1(self):
        lawbook = LawBook('Love', 1989, 5)
        self.assertTrue(lawbook.check_over_20_years)

    def test_law_book_year2(self):
        lawbook = LawBook('Hate', 2010, 10)
        self.assertFalse(lawbook.check_over_20_years())

    def test_law_book_year3(self):
        lawbook = LawBook('Normal', 2002, 10)
        self.assertFalse(lawbook.check_over_20_years())

    def test_law_book_info1(self):
        lawbook = LawBook('Love', 1989, 5)
        self.assertEqual('Law Book: Love, in-library: 5, outside-library: 0, lost: 0', lawbook.get_info())

    def test_law_book_info2(self):
        lawbook = LawBook('Love', 1989, 5)
        lawbook.borrow_book()
        lawbook.borrow_book()
        lawbook.borrow_book()
        lawbook.return_book()
        lawbook.return_book()
        lawbook.lost_book()
        self.assertEqual('Law Book: Love, in-library: 3, outside-library: 1, lost: 1', lawbook.get_info())

    def test_math_level1(self):
        mathbook = MathBook('Hello', 2020, 10, 100)
        with self.assertRaises(Exception) as e:
            mathbook.check_level()
        self.assertTrue('Wrong level' in str(e.exception))

    def test_math_level2(self):
        mathbook1 = MathBook('Hello1', 2020, 10, 0)
        mathbook2 = MathBook('Hello2', 2020, 10, 10)
        mathbook3 = MathBook('Hello3', 2020, 10, 15)
        mathbook4 = MathBook('Hello4', 2020, 10, 20)
        mathbook5 = MathBook('Hello5', 2020, 10, 30)
        mathbook6 = MathBook('Hello6', 2020, 10, 50)
        self.assertEqual("primary student's book", mathbook1.check_level())
        self.assertEqual("primary student's book", mathbook2.check_level())
        self.assertEqual("secondary student's book", mathbook3.check_level())
        self.assertEqual("secondary student's book", mathbook4.check_level())
        self.assertEqual("high school student's book", mathbook5.check_level())
        self.assertEqual("normal book", mathbook6.check_level())

    def test_math_status(self):
        mathbook = MathBook("World", 1999, 20, 13)
        mathbook.borrow_book()
        mathbook.borrow_book()
        mathbook.borrow_book()
        mathbook.return_book()
        mathbook.return_book()
        mathbook.lost_book()
        self.assertEqual("Math Book: World, level: secondary student's book, in-library: 18, outside-library: 1, lost: 1", mathbook.get_info())